import pandas as pd
import numpy as np
import typer
import sys

from utils import get_daily_tgvmax_results, _check_trajectory_multi

app = typer.Typer(add_completion=False)


def _get_stats(city=None, date=None):
    data = get_daily_tgvmax_results()
    if city is None:
        print("Running stats for all of France:")
        filtered_data = data
    else:
        print(f"Running stats for {city}")
        filtered_data = data[data["origine"] == city]

    if date is not None:
        print(f"Filtering by date {date}")
        filtered_data = filtered_data[filtered_data["date"] == date]

    n_trains = len(filtered_data)
    n_days = len(filtered_data.date.unique())
    destinations = filtered_data.destination.unique()
    n_trains_available = len(filtered_data[filtered_data["od_happy_card"] == "OUI"])
    if n_days:
        print(
            f"Total number of trains: {n_trains} ({n_trains/n_days:.2f} trains/day on average)"
        )
    if n_trains:
        print(
            f"Available Trains: {n_trains_available} ({n_trains_available*100/n_trains:.2f}%) ({n_trains_available/n_days:.2f} trains/day on average)"
        )

    print(f"Destinations: {len(destinations)}")
    max_destination_length = np.max([len(destination) for destination in destinations])
    for destination in sorted(destinations):
        trains_for_destination = filtered_data[
            filtered_data["destination"] == destination
        ]
        n_trains_for_destination = len(trains_for_destination)
        n_trains_for_destination_available = len(
            trains_for_destination[trains_for_destination["od_happy_card"] == "OUI"]
        )
        print(
            f"  {destination:<{max_destination_length}} {n_trains_for_destination_available:>3}/{n_trains_for_destination:<3}  {n_trains_for_destination_available*100/n_trains_for_destination:.2f}%"
        )
    print()


def _list_all_cities():
    data = get_daily_tgvmax_results()
    cities = sorted(np.unique(np.concatenate([data["origine"], data["destination"]])))
    print(f"Available locations: ({len(cities)} possibilities)")
    print("\n".join(cities))


@app.callback()
def main():
    """Cli to investigate tgvmax options."""
    print(sys.argv)  # Todo: Should use the typer framework?


@app.command()
def get_stats(
    city: str = typer.Option(default=None, help="Name of city to investigate"),
    date: str = typer.Option(
        default=None, help="Date to investigate (format=YYYY-MM-DD)"
    ),
):
    """
    Get statistics on tgvmax trains.
    """
    _get_stats(city=city, date=date)


@app.command()
def check_trajectory(
    origine: str = typer.Option(default="", help="Name of city to leave from"),
    destination: str = typer.Option(default="", help="Name of city to leave from"),
    date: str = typer.Option(default=None, help="Date to leave on (format=YYYY-MM-DD)"),
    filter_available: bool = typer.Option(
        default=False, help="Remove unavailable trains."
    ),
    max_wait: int = typer.Option(
        default=0,
        help="Max number of hours to wait for connection (if 0 then only direct trains are proposed).",
    ),
):
    """
    Get info on specific trajectories.
    """
    _check_trajectory_multi(
        origine=origine,
        destination=destination,
        date=date,
        filter_available=filter_available,
        max_wait=max_wait,
    )


@app.command()
def list_all_cities():
    """
    List all the possible locations accessible by train.
    """
    _list_all_cities()


def cli():
    pd.set_option("display.max_rows", None, "display.max_columns", None)
    app()


if __name__ == "__main__":
    cli()
