import datetime
import time
import pandas as pd
from pathlib import Path
import requests
from tqdm import tqdm
import warnings
from functools import wraps

warnings.filterwarnings("ignore", "This pattern has match groups")

DATA_FOLDER = Path(".sncf_data").resolve().absolute()
DATA_FOLDER.mkdir(parents=True, exist_ok=True)


def load_results(results_path):
    data = pd.read_csv(results_path, sep=";")
    # data["heure_arrivee"] = pd.to_datetime(data["heure_arrivee"])
    # data["heure_depart"] = pd.to_datetime(data["heure_depart"])
    return data


def timeit(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.time()
        func_output = func(*args, **kwargs)
        elapsed_time = time.time() - start_time
        print(f"function [{func.__name__}] finished in {elapsed_time:.3f} s")
        return func_output

    return wrapper


@timeit
def get_daily_tgvmax_results():
    results_path = DATA_FOLDER / f"tgvmax_data_{datetime.date.today()}.txt"

    if not results_path.is_file():
        print(
            f"No data found for {datetime.date.today()}, loading from opendatasoft..."
        )
        api_url = "https://data.opendatasoft.com/api/records/1.0/download/?dataset=tgvmax%40datasncf&sort=date"
        end_of_line_encoded = "\n".encode("utf-8")
        with open(results_path, "ab") as out_file:
            with requests.get(api_url, stream=True) as response:
                for line in response.iter_lines():
                    if line:
                        out_file.write(line)
                        out_file.write(end_of_line_encoded)
        print("Finished loading data.")
    return load_results(results_path)


@timeit
def _check_trajectory_multi(
    origine="", destination="", date=None, filter_available=True, max_wait=0
):
    possibilities = get_daily_tgvmax_results()
    possible_trips = []
    if filter_available:
        possibilities = possibilities[possibilities["od_happy_card"] == "OUI"]

    if date is not None:
        print(f"Filtering by date {date}")
        possibilities = possibilities[possibilities["date"] == date]

    origine_possibilities = possibilities[
        possibilities["origine"].str.contains(origine)
    ]
    end_possibilities = possibilities[
        possibilities["destination"].str.contains(destination)
    ]
    # Remove trains that are in both lists:
    end_possibilities = end_possibilities.loc[
        end_possibilities.index.difference(origine_possibilities.index),
    ]

    # get all direct trains
    mask = origine_possibilities["destination"].str.contains(destination)
    direct_trains_to_skip = []
    for index, p in origine_possibilities[mask].iterrows():
        possible_trips.append([index])
        direct_trains_to_skip.append(p["train_no"])

    # get all trains with a stop:
    if max_wait > 0:
        for index_origine, origine_possibility in tqdm(
            origine_possibilities.iterrows(), total=len(origine_possibilities)
        ):
            # Keep possibilities that are on the right date
            pos = end_possibilities[
                end_possibilities["date"] == origine_possibility["date"]
            ]
            # Keep possibilities that leave from the right station
            pos = pos[pos["origine"].str.contains(origine_possibility["destination"])]
            # Keep the possibilities with good connection time
            time_delta = pd.to_datetime(pos["heure_depart"]) - pd.to_datetime(
                origine_possibility["heure_arrivee"]
            )
            pos = pos[
                (time_delta > pd.Timedelta(hours=0))
                & (time_delta < pd.Timedelta(hours=max_wait))
            ]

            for index_destination, p in pos.iterrows():
                if p["train_no"] in direct_trains_to_skip:
                    continue
                possible_trips.append([index_origine, index_destination])

    # Approximate duration of trips TODO: store this when calculating the possibilities
    possible_trips.sort(
        reverse=True,
        key=lambda x: get_trip_length(
            possibilities.loc[[x[0]]], possibilities.loc[[x[-1]]]
        ),
    )

    if len(possible_trips):
        for trip_nb, possible_trip in enumerate(possible_trips):
            print(f"\nTrip Possibility {trip_nb+1}/{len(possible_trips)}:")
            print_trains(possibilities.loc[possible_trip])
    else:
        print("No trains found.")
    return possible_trips


def get_trip_length(first_train, last_train):
    start_time = time.strptime(first_train["heure_arrivee"].values[0], "%H:%M")
    end_time = time.strptime(last_train["heure_arrivee"].values[0], "%H:%M")
    total_time = (
        start_time.tm_hour * 60
        + start_time.tm_min
        - (end_time.tm_hour * 60 + end_time.tm_min)
    )
    return total_time


def print_trains(train_row):
    for i in range(len(train_row)):
        str_repr = (
            f"{train_row['date'].values[i]} - "
            f"FROM {train_row['origine'].values[i]} ({train_row['heure_depart'].values[i]}) "
            f"TO {train_row['destination'].values[i]} ({train_row['heure_arrivee'].values[i]}) "
            f" - TRAIN NUMBER {train_row['train_no'].values[i]}"
        )
        print(str_repr)
